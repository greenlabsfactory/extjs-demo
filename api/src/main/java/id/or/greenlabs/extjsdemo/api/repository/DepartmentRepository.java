package id.or.greenlabs.extjsdemo.api.repository;

import id.or.greenlabs.extjsdemo.api.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by krissadewo on 9/23/17.
 */
public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
