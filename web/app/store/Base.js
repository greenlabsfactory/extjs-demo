/**
 * Created by krissadewo on 06/10/17.
 * Base generic class for storing list of data
 */
Ext.define('Admin.store.Base', {
    extend: 'Ext.data.Store',
    pageSize: 10,
    schema: {
        namespace: 'Admin.store'
    },
    listeners: {
        /**
         Available since: 1.1.0 PARAMETERS
         store :  Ext.data.Store
         This Store
         operation :  Ext.data.operation.Operation
         The Ext.data.operation.Operation object that will be passed to the Proxy to load the Store
         eOpts : Object
         The options object passed to Ext.util.Observable.addListener.

         @param store
         @param operation
         @param eOpts
         */
        beforeLoad: function (store, operation, eOpts) {
            this.getProxy().headers = {
                "Authorization": "Bearer " + Admin.proxy.Api.token
            };

            this.getProxy().extraParams = {
                offset: (store.currentPage - 1) * 10,
                limit: 10
            };
        }
    },
});
