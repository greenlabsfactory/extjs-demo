package id.or.greenlabs.extjsdemo.api.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by krissadewo on 9/23/17.
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
public class Department implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    //@JsonIgnoreProperties(ignoreUnknown=true)
    //@JsonManagedReference
    @OneToMany(mappedBy = "department")
    private List<Employee> employees = new ArrayList<>();

    public Department(Long id) {
        this.id = id;
    }

    public Department(String name) {
        this.name = name;
    }
}
