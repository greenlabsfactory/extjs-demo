/**
 * Created by krissadewo on 07/09/17.
 */
Ext.define('Admin.model.Employee', {
    extend: 'Admin.model.Base',
    alias: 'model.employee',
    idProperty: 'id',
    proxy: Admin.proxy.Api.go(this, Api.resource.EMPLOYEE),
    fields: [
        {
            type: 'number',
            name: 'id',
            useNull: true
        },
        {
            type: 'string',
            name: 'firstName'
        },
        {
            type: 'string',
            name: 'lastName'
        },
        {
            type: 'string',
            name: 'email'
        },
        {
            type: 'int',
            name: 'age'
        },
        {
            type: 'date',
            name: 'registrationDate'
        },
        {
            type: 'date',
            name: 'createdTime',
            defaultValue: new Date()
        },
        {
            name: 'departmentId',
            reference: 'Admin.model.Department'
        }
    ]

});

