/**
 * Created by krissadewo on 07/09/17.
 */
Ext.define('Admin.model.User', {
    extend: 'Admin.model.Base',
    fields: [
        {
            type: 'number',
            name: 'id',
            persist: false
        },
        {
            type: 'string',
            name: 'username'
        },
        {
            type: 'string',
            name: 'password'
        },
        {
            type: 'string',
            name: 'profileName'
        },
        {
            type: 'string',
            name: 'status'
        },
        {
            type: 'string',
            name: 'imageCover'
        },
        {
            type: 'string',
            name: 'imageProfile'
        },
        {
            type: 'string',
            name: 'token'
        }
    ]
});

/*var loginApi = new Ext.data.proxy.Rest({
    type: 'rest's,
    url: 'http://192.168.0.9:8080/lumbung-komunitas-api/do-login',
    method: 'POST',
    appendId: false,
    useDefaultXhrHeader: false,
    actionMethods: {
        create: 'POST',
        read: 'POST',
        update: 'POST',
        destroy: 'DELETE'
    },
    reader: {
        type: 'json',
        rootProperty: 'data',
        statusProperty: 'status',
        model: 'User'
    },
    writer: {
        type: 'json',
        model: 'User'
    },
    paramsAsJson: true,
    noCache: false,
    limitParam: undefined,
    pageParam: undefined,
    startParam: undefined
});*/

