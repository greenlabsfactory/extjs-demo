/**
 * Created by krissadewo on 07/09/17.
 */
Ext.define('Admin.store.EmployeeStore', {
    extend: 'Admin.store.Base',
    storeId: 'employee',
    alias: 'store.employee',
    model: 'Admin.model.Employee',
    proxy: Admin.proxy.Api.go(this.model, Api.resource.EMPLOYEE),
    autoLoad: true,
    pageSize: 10,
    sorters: {
        direction: 'DESC',
        property: 'age'
    }
});
