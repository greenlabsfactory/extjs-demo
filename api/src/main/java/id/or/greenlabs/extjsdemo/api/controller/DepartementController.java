package id.or.greenlabs.extjsdemo.api.controller;

import id.or.greenlabs.extjsdemo.api.common.SyncCallback;
import id.or.greenlabs.extjsdemo.api.entity.Department;
import id.or.greenlabs.extjsdemo.api.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by krissadewo on 9/29/17.
 */
@RestController
public class DepartementController extends BaseController {

    @Autowired
    private DepartmentService service;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("departments")
    public SyncCallback find(@RequestParam(value = "offset", required = false) Integer offset,
                             @RequestParam(value = "limit", required = false) Integer limit) {

        Department param = new Department();
        return syncCallback.getCallback(service.find(param, offset, limit), service.count(param));
    }

}
