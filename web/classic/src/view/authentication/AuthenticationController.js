Ext.define('Admin.view.authentication.AuthenticationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.authentication',

    onFaceBookLogin: function () {
        Admin.constants.TIMEOUT = 0;
        Ext.util.Cookies.set("isLogin", true);
        Ext.create('Admin.data', {isLogin: true}).save();

        this.redirectTo('dashboard', true);
    },

    onLoginButton: function () {
        Admin.constants.TIMEOUT = 0;
        var me = this;
        var login = Ext.create('Admin.model.Login', {
            username: this.getViewModel().get('username'),
            password: this.getViewModel().get('password')
        });

        Admin.proxy.Api.login(login.data, me);
    },

    onLoginAsButton: function () {
        this.redirectTo('login', true);
    },

    onNewAccount: function () {
        this.redirectTo('register', true);
    },

    onSignupClick: function () {
        this.redirectTo('dashboard', true);
    },

    onResetClick: function () {
        this.redirectTo('dashboard', true);
    }
});