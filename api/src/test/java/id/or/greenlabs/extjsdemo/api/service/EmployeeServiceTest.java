package id.or.greenlabs.extjsdemo.api.service;

import id.or.greenlabs.extjsdemo.api.ExtjsDemoApplication;
import id.or.greenlabs.extjsdemo.api.ExtjsDemoApplicationTests;
import id.or.greenlabs.extjsdemo.api.entity.Department;
import id.or.greenlabs.extjsdemo.api.entity.Employee;
import id.or.greenlabs.extjsdemo.api.common.Result;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.util.List;

/**
 * Created by krissadewo on 9/23/17.
 */
@EnableAutoConfiguration
public class EmployeeServiceTest extends ExtjsDemoApplicationTests {

    @Autowired
    private EmployeeService service;
    @Autowired
    private DepartmentService departmentService;
    private Employee employee;

    @Before
    public void init() {
        if (employee == null) {
            save();
        }
    }

    private void save() {
        Result result = service.save(dummyData.generateEmployee(departmentService.find(new Department(), 0, 1).get(0)));
        employee = result.getEmployee();

        Assert.assertNotNull(employee.getId());
    }

    @Test
    public void findById() {
        Employee result = service.findById(employee.getId());
        Assert.assertEquals(employee.getId(), result.getId());
    }

    @Test
    public void find() {
        List<Employee> employees = service.find(employee, 0, 10);

        Assert.assertNotNull(employees);
        Assert.assertTrue(employees.size() > 0);

        System.out.println(toJson(employees));
        ExtjsDemoApplication.getLogger(this).debug(toJson(employees));
    }

}
