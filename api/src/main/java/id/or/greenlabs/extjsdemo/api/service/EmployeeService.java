package id.or.greenlabs.extjsdemo.api.service;

import id.or.greenlabs.extjsdemo.api.ExtjsDemoApplication;
import id.or.greenlabs.extjsdemo.api.common.Result;
import id.or.greenlabs.extjsdemo.api.entity.Employee;
import id.or.greenlabs.extjsdemo.api.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by krissadewo on 9/23/17.
 */
@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    /**
     * @param entity
     * @return
     */
    public Result save(Employee entity) {
        try {
            Employee employee = repository.save(entity);
            return new Result(Result.SAVE_SUCCESS, employee);
        } catch (DataAccessException e) {
            ExtjsDemoApplication.getLogger(this).error(e.getMessage());
        }

        return new Result(Result.SAVE_FAILED);
    }

    /**
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    public Employee findById(Long id) {
        return repository.findOne(id);
    }

    /**
     * @param entity
     * @param limit
     * @param offset
     * @return
     */
    @Transactional(readOnly = true)
    public List<Employee> find(Employee entity, int offset, int limit) {
        Pageable pageable = new PageRequest(offset / limit, limit);

        Employee param = new Employee();
        param.setCreatedTime(null);
        if (entity.getFirstName() != null) {
            param.setFirstName(entity.getFirstName());
        }

        return repository.findAll(Example.of(param), pageable).getContent();
    }

    public long count(Employee entity) {
        Employee param = new Employee();
        param.setCreatedTime(null);
        if (entity.getFirstName() != null) {
            param.setFirstName(entity.getFirstName());
        }

        return repository.count(Example.of(param));
    }

    @Transactional
    public Result delete(Long id) {
        try {
            repository.delete(id);
            return new Result(Result.DELETE_SUCCESS);
        } catch (DataAccessException e) {
            ExtjsDemoApplication.getLogger(this).error(e.getMessage());
        }

        return new Result(Result.DELETE_FAILED);
    }

}
