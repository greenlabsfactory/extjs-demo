/**
 * Created by krissadewo on 08/09/17.
 */
Ext.define('Admin.view.employee.EmployeeViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.employee',
    stores: {
        employee: {
            type: 'employee'
        },

        department: {
            type: 'department'
        }
    }
});