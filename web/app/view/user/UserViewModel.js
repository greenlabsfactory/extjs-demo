/**
 * Created by krissadewo on 08/09/17.
 */
Ext.define('Admin.view.user.UserViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.user',

/*
    data: {
        username : '',
        fullName : '',
        password : '',
        email    : ''
    },
*/

    stores: {
        user: {
            type: 'user'
        }
    }
});