package id.or.greenlabs.extjsdemo.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.or.greenlabs.extjsdemo.api.util.JsonDateDeserializer;
import id.or.greenlabs.extjsdemo.api.util.JsonDateSerializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by krissadewo on 9/20/17.
 */
@Entity
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class User implements UserDetails, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "BIGINT(20) UNSIGNED")
    private Long id;

    @Column(length = 100, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(length = 100, unique = true)
    private String email;

    private String phone1;

    private String phone2;

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    @Transient
    private String token;

    @JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
    @Column(name = "created_time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime = new Date();

    @Version
    private Long version;

    @Enumerated(EnumType.STRING)
    @Column(length = 50)
    private Role role;

    //for security config
    @Transient
    private boolean enabled;

    @JsonProperty("grant_type")
    @Transient
    private String grantType = "password";

    @JsonProperty("client_id")
    @Transient
    private String clientId = "fooClientIdPassword";

    public User(String username) {
        this.username = username;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(getRole().value));
        return grantedAuthorities;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public enum Role {
        OFFER("ROLE_ADMIN"),
        NEGOTIATION("ROLE_USER");

        String value;

        Role(String value) {
            this.value = value;
        }
    }

}
