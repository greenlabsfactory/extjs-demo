/**
 * Form helper is singleton, to resolve
 * task generate widget form at runtime.
 */

Ext.define('Admin.helper.Form', {
  singleton: true,

  generateItems: function (params) {
    var items = []
    for (var i = 0; i < params.count(); i++) {
      var name = params.getAt(i).get('name')
      var prompt = params.getAt(i).get('prompt')
      var type = params.getAt(i).get('type')
      var widget = this.generateWidget(name, prompt, type)

      items.push(widget)
    }

    return items
  },

  generateWidget: function (name, label, type) {
    var widget = null
    if (type === 'textfield') {
      widget = Ext.create({
        xtype: 'textfield',
        name: name,
        fieldLabel: label,
        labelWidth: 150,
        width: 400,
        allowBlank: false
      })
    } else if (type === 'datefield') {
      widget = Ext.create({
        xtype: 'datefield',
        name: name,
        fieldLabel: label,
        labelWidth: 150,
        width: 400,
        format: 'd F Y',
        maxValue: new Date(),
        allowBlank: false
      })
    }

    return widget
  }
})
