package id.or.greenlabs.extjsdemo.api.controller;

import id.or.greenlabs.extjsdemo.api.common.SyncCallback;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by krissadewo on 9/24/17.
 */
public class BaseController {

    @Autowired
    SyncCallback syncCallback;
}
