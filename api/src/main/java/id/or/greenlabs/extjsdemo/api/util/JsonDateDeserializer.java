package id.or.greenlabs.extjsdemo.api.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by krissadewo on 05/06/17.
 */
public class JsonDateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String date = jsonParser.getText().trim();
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            formatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss");
            try {
                return formatter.parse(date);
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }

        return null;
    }
}
