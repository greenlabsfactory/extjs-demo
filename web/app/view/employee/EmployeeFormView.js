/**
 * Created by krissadewo on 27/09/17.
 */
Ext.define('Admin.view.employee.EmployeeForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.employeeform',
    closable: true,
    modal: true,
    scope: this,
    frame: false,
    title: 'Employee Form',
    resizable: false,
    controller: 'employeeform',
    model: 'employee',
    listeners: {
        show: 'onShow',
        close: 'onCancel'
    },

    items: [{
        xtype: 'form',
        itemId: 'employeeform',
        bodyPadding: '5 5 0',
        width: 600,
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },

        defaults: {
            border: false,
            layout: 'anchor',
            width: '95%'
        },

        layout: 'vbox',
        items: [
            {
                xtype: 'textfield',
                fieldLabel: 'First Name',
                name: 'firstName',
                anchor: '100%',
                bind: '{employee.firstName}',
                allowBlank: false
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Last Name',
                anchor: '100%',
                name: 'lastName',
                bind: '{employee.lastName}',
                allowBlank: false
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Age',
                anchor: '-5',
                name: 'age',
                bind: '{employee.age}'
            }, {
                xtype: 'textfield',
                fieldLabel: 'Email',
                anchor: '100%',
                name: 'email',
                vtype: 'email',
                bind: '{employee.email}'
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Registration Date',
                format: 'd F Y',
                name: 'registrationDate',
                bind: '{employee.registrationDate}',
                allowBlank: false,
                maxValue: new Date()
            },
            {
                xtype: 'combobox',
                fieldLabel: 'Department',
                name: 'department',
                store: {
                    type: 'department'
                },
                valueField: 'id',
                displayField: 'name',
                allowBlank: false,
                typeAhead: true,
                queryMode: 'local',
                bind: '{employee.department.id}',
                emptyText: 'Select a department...'
            }
        ],

        validators: {
            age: 'presence',
            firstName: {type: 'length', min: 2},
            lastName: {type: 'length', min: 2},
            department: {type: 'length', min: 2}
        },

        buttons: ['->', {
            text: 'Save',
            handler: 'onSave'
        }, {
            text: 'Cancel',
            handler: 'onCancel'
        }]

    }]

});
