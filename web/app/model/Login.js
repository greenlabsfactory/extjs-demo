/**
 * Created by krissadewo on 07/09/17.
 */
Ext.define('Admin.model.Login', {
    extend: 'Admin.model.Base',
    restApi: Ext.manifest.restApi,
    fields: [
        {
            type: 'number',
            name: 'id',
            persist: false
        },
        {
            type: 'string',
            name: 'username'
        },
        {
            type: 'string',
            name: 'password'
        },
        {
            type: 'string',
            name: 'grant_type',
            defaultValue: 'password'
        },
        {
            type: 'string',
            name: 'client_id',
            defaultValue: 'fooClientIdPassword'
        }
    ]
});

/*var loginApi = new Ext.data.proxy.Rest({
    type: 'rest',
    url: 'http://localhost:8080/oauth/token',
    method: 'POST',
    appendId: false,
    useDefaultXhrHeader: false,
    actionMethods: {
        create: 'POST',
        read: 'POST',
        update: 'POST',
        destroy: 'DELETE'
    },
    reader: {
        type: 'json',
        rootProperty: 'data',
        statusProperty: 'status',
        model: 'Login'
    },
    writer: {
        type: 'json',
        model: 'Login'
    },
    paramsAsJson: true,
    noCache: false,
    limitParam: undefined,
    pageParam: undefined,
    startParam: undefined
});*/

