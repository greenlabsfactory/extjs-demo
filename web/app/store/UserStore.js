/**
 * Created by krissadewo on 07/09/17.
 */
Ext.define('Admin.store.UserStore', {
    extend: 'Ext.data.Store',
    alias: 'store.user',
    model: 'Admin.model.User',
    storeId: 'userStore',
    autoLoad: true,
    pageSize: 10,
    listeners: {
        beforeLoad: function (tbar, pageData, eOpts) {
            //debugger;
            this.getProxy().extraParams = {
                offset: (tbar.currentPage - 1) * 10,
                limit: 10
            };
        }
    },

    sorters: {
        direction: 'DESC',
        property: 'online'
    }

    /* proxy: {
     type: 'rest',
     url: 'http://192.168.0.9:8080/lumbung-komunitas-api/users',
     method: 'GET',
     appendId: false,
     useDefaultXhrHeader: false,
     reader: {
     type: 'json',
     rootProperty: 'data',
     statusProperty: 'status',
     model: 'User'
     },
     writer: {
     type: 'json',
     model: 'User'
     },
     paramsAsJson: true,
     noCache: false,
     limitParam: undefined,
     pageParam: undefined,
     startParam: undefined
     }*/

});
