# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* This application consist of some module :
* api => spring boot apps (orm, security) 
* web => extjs

### How do I get set up? ###

* Summary of set up
* api => maven, intelliJ,
* run with :  ./mvnw spring-boot:run -DskipTests=true

* web => extjs
* run with : sencha app watch

### Contribution guidelines ###

* Fork a GitHub repository.
* Clone the forked repository to your local system.
* Add a Git remote for the original repository.
* Create a feature branch in which to place your changes.
* Make your changes to the new branch.
* Commit the changes to the branch.
* Push the branch to GitHub.
* Open a pull request from the new branch to the original repo.
* Clean up after your pull request is merged.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact