package id.or.greenlabs.extjsdemo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import id.or.greenlabs.extjsdemo.api.common.DummyData;
import id.or.greenlabs.extjsdemo.api.entity.Employee;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan(basePackages = {
        "id.or.greenlabs.extjsdemo.api.service",
        "id.or.greenlabs.extjsdemo.api.common"})
@EnableAutoConfiguration
public class ExtjsDemoApplicationTests extends SpringBootServletInitializer {

    @Autowired
    public DummyData dummyData;

    @Autowired
    protected WebApplicationContext applicationContext;

    protected Gson gson = new GsonBuilder()
            .setDateFormat("MMM dd, yyyy hh:mm:ss a")
            //.registerTypeAdapter(Date.class, new JsonDateDeserializer())
            //.registerTypeAdapter(Date.class, new JsonDateSerializer())
            .create();
    protected MockMvc mockMvc;

    @Before
    public void init() throws Exception {
        if (mockMvc == null) {
            MockitoAnnotations.initMocks(this);
            mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
        }
    }

    @Test
    public void contextLoads() {
    }

    public Object getData(String json){
        Map map = fromJson(json, Map.class);
        return fromJson(toJson(map.get("data")), Employee.class);
    }

    public <T> T fromJson(String json, Class<T> classOfT) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        //objectMapper.registerModule(new Hibernate5Module());
        try {
            return objectMapper.readValue(json, classOfT);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String toJson(Object object) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.registerModule(new Hibernate5Module());
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

}
