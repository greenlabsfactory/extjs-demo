/**
 * Created by krissadewo on 11/09/17.
 */
Ext.define('Admin.view.common.ToolbarView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.customtoolbar',
    controller: 'customtoolbar',
    requires: [
        'Ext.ux.BoxReorderer'
    ],

    items: [
        {
            tbar: {
                plugins: 'boxreorderer',
                itemId: 'toolbarMenu',
                defaults: {
                    reorderable: true
                },

                layout: {
                    pack: 'end',
                    type: 'hbox'
                }

                /*items: [{
                    xtype: 'splitbutton',
                    text: 'Menu Button',
                    iconCls: null,
                    glyph: 61,
                    reorderable: false,
                    menu: [{
                        text: 'Menu Button 1'
                    }]
                }, {
                    xtype: 'splitbutton',
                    text: 'Cut',
                    iconCls: null,
                    glyph: 61,
                    menu: [{
                        text: 'Cut Menu Item'
                    }]
                }, {
                    iconCls: null,
                    glyph: 61,
                    text: 'Copy'
                }, {
                    text: 'Paste',
                    iconCls: null,
                    glyph: 61,
                    menu: [{
                        text: 'Paste Menu Item'
                    }]
                }, {
                    iconCls: null,
                    glyph: 61,
                    text: 'Format'

                }]*/
            }
        }
    ]


});
