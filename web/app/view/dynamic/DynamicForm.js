/**
 * Array field for Employee
 * @type {Ext.data.Store}
 */
var fields = Ext.create('Ext.data.Store', {
  fields: ['name', 'prompt', 'type'],
  data:
    [
      {'name': 'firstName', 'prompt': 'First Name', 'type': 'textfield'},
      {'name': 'lastName', 'prompt': 'Last Name', 'type': 'textfield'},
      {'name': 'age', 'prompt': 'Age', 'type': 'textfield'},
      {'name': 'email', 'prompt': 'Email', 'type': 'textfield'},
      {'name': 'registrationDate', 'prompt': 'Registration Date', 'type': 'datefield'},
    ]
})

/**
 * Dynamic form example for Employee
 */

Ext.define('Admin.dynamic.DynamicForm', {
  extend: 'Ext.form.Panel',
  xtype: 'dynamicForm',
  alias: 'widget.dynamicForm',
  title: 'Dynamic Form',
  id: 'dynamicForm',
  bodyPadding: 15,
  autoScroll: true,
  layout: 'auto',
  items: Admin.helper.Form.generateItems(fields),
  defaults: {anchor: '100%'},
  fieldDefaults: {
    labelAlign: 'top',
    msgTarget: 'side'
  },
  buttons: [
    {
      text: 'Reset',
      handler: function () {
        this.up('form').getForm().reset()
      }
    },
    {
      text: 'Save',
      formBind: true,
      disabled: true,
      handler: function () {
        var form = this.up('form').getForm()
        if (form.isValid()) {
          form.reset()
          alert('Successfully...')
        }
      }
    }
  ]
})
