/**
 * Created by krissadewo on 08/09/17.
 */
Ext.define('Admin.view.common.ToolbarController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.customtoolbar',
    init: function () {
        this.setupView();
    },

    setupView: function () {
        var toolbarMenu = this.getView().down('#toolbarMenu');
        toolbarMenu.add(
            Ext.create({
                xtype: 'button',
                text: 'Add',
                itemId: 'addButton',
                iconCls: null,
                glyph: 61
            })
        );

        toolbarMenu.add(
            Ext.create({
                xtype: 'button',
                text: 'Edit',
                itemId: 'editButton',
                iconCls: null,
                glyph: 61
            })
        );

        toolbarMenu.add(
            Ext.create({
                xtype: 'button',
                text: 'Delete',
                itemId: 'deleteButton',
                iconCls: null,
                glyph: 61
            })
        );

        toolbarMenu.add(
            Ext.create({
                xtype: 'button',
                text: 'Search',
                itemId: 'searchButton',
                iconCls: null,
                glyph: 61
            })
        );
    }

});
