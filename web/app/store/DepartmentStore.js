/**
 * Created by krissadewo on 07/09/17.
 */
Ext.define('Admin.store.DepartmentStore', {
    extend: 'Admin.store.Base',
    storeId: 'department',
    alias: 'store.department',
    model: 'Admin.model.Department',
    proxy: Admin.proxy.Api.go(this.model, Api.resource.DEPARTMENT),
    autoLoad: true,
    pageSize: 10
});
