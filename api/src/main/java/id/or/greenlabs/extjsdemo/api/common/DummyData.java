package id.or.greenlabs.extjsdemo.api.common;

import id.or.greenlabs.extjsdemo.api.entity.Employee;
import id.or.greenlabs.extjsdemo.api.entity.Department;
import org.springframework.stereotype.Component;

/**
 * Created by krissadewo on 9/23/17.
 */
@Component
public class DummyData {

    public Department generateDepartement(){
       return new Department("HRD");
    }

    public Employee generateEmployee(Department department){
        Employee employee = new Employee();
        employee.setFirstName("kris");
        employee.setLastName("sadewo");
        employee.setDepartment(department);
        return employee;
    }
}
