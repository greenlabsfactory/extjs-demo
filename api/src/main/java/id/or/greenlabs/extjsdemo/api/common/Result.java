package id.or.greenlabs.extjsdemo.api.common;

import id.or.greenlabs.extjsdemo.api.entity.Department;
import id.or.greenlabs.extjsdemo.api.entity.Employee;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by krissadewo on 9/23/17.
 */
@Getter
@NoArgsConstructor
public class Result {

    public static final String SAVE_SUCCESS = "001";
    public static final String SAVE_FAILED = "100";

    public static final String DELETE_SUCCESS = "011";
    public static final String DELETE_FAILED = "110";

    private String message;
    private Employee employee;
    private Department department;

    public Result(String message) {
        this.message = message;
    }

    public Result(String message, Employee employee) {
        this.message = message;
        this.employee = employee;
    }

    public Result(String message, Department department) {
        this.message = message;
        this.department = department;
    }
}
