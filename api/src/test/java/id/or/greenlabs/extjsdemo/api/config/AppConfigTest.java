package id.or.greenlabs.extjsdemo.api.config;

import id.or.greenlabs.extjsdemo.api.common.DummyData;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by krissadewo on 9/23/17.
 */
@Configuration
public class AppConfigTest {

    @Bean
    public DummyData dummyData() {
        return new DummyData();
    }
}
