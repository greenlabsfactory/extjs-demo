package id.or.greenlabs.extjsdemo.api.controller;

import id.or.greenlabs.extjsdemo.api.ExtjsDemoApplicationTests;
import id.or.greenlabs.extjsdemo.api.entity.Employee;
import id.or.greenlabs.extjsdemo.api.service.DepartmentService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Created by krissadewo on 9/24/17.
 */
@EnableAutoConfiguration
public class EmployeeControllerTest extends ExtjsDemoApplicationTests {

    Employee employee;

    @Autowired
    private DepartmentService departmentService;

    @Before
    public void init() throws Exception {
        super.init();
        if (employee == null) {
            save();
        }
    }

    private void save() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/employees")
                .content(gson.toJson(dummyData.generateEmployee(departmentService.find(null, 0, 1).get(0))))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        employee = (Employee) getData(result.getResponse().getContentAsString());

        Assert.assertNotNull(employee);
        Assert.assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
    }

    @Test
    public void find() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .param("limit", String.valueOf(10))
                .param("offset", String.valueOf(0)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        Assert.assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
        result.getResponse().getContentAsString();

    }

}