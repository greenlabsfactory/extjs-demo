package id.or.greenlabs.extjsdemo.api.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by krissadewo on 05/06/17.
 */
public class JsonDateSerializer extends StdSerializer<Date> {

    static final SimpleDateFormat JSON_SERIALIZE_DATE_FORMAT = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");

    public JsonDateSerializer() {
        this(null);
    }

    public JsonDateSerializer(Class<Date> t) {
        super(t);
    }

    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value == null) {
            gen.writeNull();
        } else {
            gen.writeString(JSON_SERIALIZE_DATE_FORMAT.format(value.getTime()));
        }
    }
}
