package id.or.greenlabs.extjsdemo.api.service;

import id.or.greenlabs.extjsdemo.api.ExtjsDemoApplication;
import id.or.greenlabs.extjsdemo.api.common.Result;
import id.or.greenlabs.extjsdemo.api.entity.Department;
import id.or.greenlabs.extjsdemo.api.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by krissadewo on 9/29/17.
 */
@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository repository;

    @PostConstruct
    public void initDepartment(){
        this.save(new Department("R&D"));
        this.save(new Department("HRD"));
        this.save(new Department("FINANCE"));
        this.save(new Department("GL"));
    }

    /**
     * @param entity
     * @return
     */
    public Result save(Department entity) {
        try {
            Department department = repository.save(entity);
            return new Result(Result.SAVE_SUCCESS, department);
        } catch (DataAccessException e) {
            ExtjsDemoApplication.getLogger(this).error(e.getMessage());
        }

        return new Result(Result.SAVE_FAILED);
    }

    /**
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    public Department findById(Long id) {
        return repository.findOne(id);
    }

    /**
     * @param entity
     * @param limit
     * @param offset
     * @return
     */
    @Transactional(readOnly = true)
    public List<Department> find(Department entity, int offset, int limit) {
        Pageable pageable = new PageRequest(offset / limit, limit);
        return repository.findAll(Example.of(entity), pageable).getContent();
    }

    public long count(Department entity) {
        return repository.count(Example.of(entity));
    }

    @Transactional
    public Result delete(Long id) {
        try {
            repository.delete(id);
            return new Result(Result.DELETE_SUCCESS);
        } catch (DataAccessException e) {
            ExtjsDemoApplication.getLogger(this).error(e.getMessage());
        }

        return new Result(Result.DELETE_FAILED);
    }

}
