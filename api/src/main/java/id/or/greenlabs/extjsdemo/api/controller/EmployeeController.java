package id.or.greenlabs.extjsdemo.api.controller;

import id.or.greenlabs.extjsdemo.api.common.Result;
import id.or.greenlabs.extjsdemo.api.common.SyncCallback;
import id.or.greenlabs.extjsdemo.api.entity.Employee;
import id.or.greenlabs.extjsdemo.api.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by krissadewo on 9/24/17.
 */
@RestController
public class EmployeeController extends BaseController {

    @Autowired
    private EmployeeService service;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("employees")
    public SyncCallback find(@RequestParam(value = "id", required = false) Long id,
                             @RequestParam(value = "offset", required = false) Integer offset,
                             @RequestParam(value = "limit", required = false) Integer limit) {
        if (id != null) {
            return syncCallback.getCallback(service.findById(id));
        } else {
            Employee param = new Employee();
            return syncCallback.getCallback(service.find(param, offset, limit), service.count(param));
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping("employees")
    public SyncCallback save(@RequestBody Employee employee) {
        Result result = service.save(employee);
        return syncCallback.getCallback(result.getMessage(), result.getEmployee());
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping("employees")
    public SyncCallback update(@RequestBody Employee employee) {
        Result result = service.save(employee);
        return syncCallback.getCallback(result.getMessage(), result.getEmployee());
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("employees/{id}")
    public SyncCallback delete(@PathVariable Long id) {
        Result result = service.delete(id);
        return syncCallback.getCallback(result.getMessage());
    }
}
