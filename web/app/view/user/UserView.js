/**
 * Created by krissadewo on 08/09/17.
 */
Ext.define('Admin.view.user.User', {
    extend: 'Ext.container.Container',
    xtype: 'user',
    alias: 'widget.user',
    controller: 'user',
    margin: '20 20 0 20',
    viewModel: {
        type: 'user'
    },

    items: [
        {
            xtype: 'panel',
            title: 'User',
            frame: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    itemId: 'toolbar'

                },
                {
                    xtype: 'grid',
                    cls: 'email-inbox-panel shadow',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },

                    selModel: {
                        selType: 'checkboxmodel',
                        checkOnly: true,
                        showHeaderCheckbox: true
                    },

                    bind: {
                        store: '{user}'
                    },

                    viewConfig: {
                        preserveScrollOnRefresh: true,
                        preserveScrollOnReload: true,
                        trackOver: false,
                        stripeRows: false
                    },

                    listeners: {
                        cellclick: 'onGridCellItemClick'
                    },

                    columns: [
                        {
                            dataIndex: '',
                            menuDisabled: true,
                            text: '<span class="x-fa fa-heart"></span>',
                            width: 40,
                            renderer: function (value) {
                                return '<span class="x-fa fa-heart' + (value ? '' : '-o') + '"></span>';
                            }
                        },
                        {
                            dataIndex: 'username',
                            text: 'User Name',
                            width: 160,
                            align: 'left'
                        },
                        {
                            dataIndex: 'profileName',
                            text: 'Profile Name',
                            width: 160,
                            align: 'left'
                        },
                        {
                            dataIndex: 'imageCover',
                            text: 'Image Cover',
                            flex: 1
                        },
                        {
                            dataIndex: 'has_attachments',
                            text: '<span class="x-fa fa-paperclip"></span>',
                            width: 40,
                            renderer: function (value) {
                                return value ? '<span class="x-fa fa-paperclip"></span>' : '';
                            }
                        },
                        {
                            xtype: 'datecolumn',
                            dataIndex: 'received_on',
                            width: 90,
                            text: 'Received'
                        }
                    ],

                    bbar: {
                        xtype: 'pagingtoolbar',
                        displayInfo: true,
                        displayMsg: 'Displaying topics {0} - {1} of {2}',
                        emptyMsg: "No topics to display",
                    }
                }
            ]

        }
    ],



});
