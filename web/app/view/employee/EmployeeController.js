/**
 * Created by krissadewo on 27/09/17.
 */
Ext.define('Admin.view.employee.EmployeeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.employee',
    store: null,
    init: function () {
        this.setupView('employee');
    },

    initViewModel: function () {
        this.grid = Ext.getCmp("grid"); //send to form employee
    },

    setupView: function (view, params) {
        var toolbar = Ext.create('Admin.view.common.ToolbarView');
        toolbar.down('#addButton').addListener('click', this.onAdd, this);
        toolbar.down('#editButton').addListener('click', this.onEdit, this);
        toolbar.down('#deleteButton').addListener('click', this.onDelete, this);

        var toolbarMenu = this.getView().down('#toolbar');
        toolbarMenu.add(toolbar);
    },

    onSelectionChange: function (grid, selectedRecord, index) {

    },

    onAdd: function () {
        var widget = Ext.create("widget.employeeform");
        widget.controller.grid = this.grid;
        widget.show();
    },

    onEdit: function () {
        var widget = Ext.create("widget.employeeform");
        widget.controller.grid = this.grid;
        widget.show();
    },

    onDelete: function (btn) {
        var me = this;
        if (!this.grid || this.grid.getSelection().length === 0) {
            return;
        }

        Ext.MessageBox.show({
            title: 'Save Changes?',
            msg: 'You are closing a tab that has unsaved changes. <br />Would you like to save your changes?',
            buttons: Ext.MessageBox.YESNO,
            scope: this,
            fn: function (btn) {
                me.onMessageBoxAction(btn);
            },

            icon: Ext.MessageBox.QUESTION
        });
    },

    onMessageBoxAction: function (btn) {
        var me = this;
        if (btn === "yes") {
            var data = this.grid.getSelection()[0].data;
            Admin.proxy.Api.delete('model.employee', data, {
                callback: function (record, operation, success) {
                    console.log(success + " data from server " + record['data'] + " : " + record['status']);
                    if (success) {
                        if (me.grid) {
                            me.grid.store.reload();
                        }
                    } else {
                        console.log(operation);
                    }
                }
            });
        }
    },

    onSearch: function () {
    }
});
