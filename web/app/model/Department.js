/**
 * Created by krissadewo on 07/09/17.
 */
Ext.define('Admin.model.Department', {
    extend: 'Admin.model.Base',
    alias: 'model.department',
    idProperty: 'id',
    fields: [
        {
            type:'number',
            name: 'id',
            useNull: true
        },
        {
            type: 'string',
            name: 'name'
        }
    ]

});

