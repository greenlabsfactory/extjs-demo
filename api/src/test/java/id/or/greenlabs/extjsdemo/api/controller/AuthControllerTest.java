package id.or.greenlabs.extjsdemo.api.controller;

import id.or.greenlabs.extjsdemo.api.ExtjsDemoApplicationTests;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * Created by krissadewo on 9/25/17.
 */
@EnableAutoConfiguration
public class AuthControllerTest extends ExtjsDemoApplicationTests {

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private static final String CLIENT_ID = "fooClientIdPassword";
    private static final String CLIENT_SECRET = "secret";
    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";
    private static final String EMAIL = "k.sadewo@greenlabs.or.id";
    private static final String NAME = "k.sadewo";

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.applicationContext)
                .addFilter(springSecurityFilterChain).build();
    }

    private String obtainAccessToken(String username, String password) throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        params.add("client_id", CLIENT_ID);
        params.add("username", username);
        params.add("password", password);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post("/oauth/token")
                .params(params)
                .with(SecurityMockMvcRequestPostProcessors.httpBasic(CLIENT_ID, CLIENT_SECRET))
                .accept("application/json;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;charset=UTF-8"));

        String resultString = result.andReturn().getResponse().getContentAsString();

        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("access_token").toString();
    }

    @Test
    public void givenNoTokenWhenGetSecureRequestThenUnauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/employee").param("email", EMAIL))
                .andExpect(MockMvcResultMatchers.status().isNonAuthoritativeInformation());
    }

    @Test
    public void givenInvalidRoleWhenGetSecureRequestThenForbidden() throws Exception {
        final String accessToken = obtainAccessToken("user1", "pass");
        System.out.println("token:" + accessToken);
        mockMvc.perform(MockMvcRequestBuilders.get("/employee")
                .header("Authorization", "Bearer " + accessToken).param("email", EMAIL))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void givenTokenWhenPostGetSecureRequestThenOk() throws Exception {
        final String accessToken = obtainAccessToken("admin", "nimda");
        mockMvc.perform(MockMvcRequestBuilders.get("/employees")
                .param("limit", String.valueOf(10))
                .param("offset", String.valueOf(0))
                .header("Authorization", "Bearer " + accessToken)
                .accept(CONTENT_TYPE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(NAME)));

    }
}
