package id.or.greenlabs.extjsdemo.api.repository;

import id.or.greenlabs.extjsdemo.api.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by krissadewo on 9/25/17.
 */
public interface UserRepository extends JpaRepository<User, Long> {
}
