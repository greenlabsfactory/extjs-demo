Ext.define('Admin.store.NavigationTree', {
  extend: 'Ext.data.TreeStore',
  storeId: 'NavigationTree',
  fields: [{
    name: 'text'
  }],

  root: {
    expanded: true,
    children: [
      {
        text: 'Profile',
        iconCls: 'x-fa fa-desktop',
        rowCls: 'nav-tree-badge',
        viewType: 'profile',
        leaf: true
      },
      {
        text: 'Employee',
        iconCls: 'x-fa fa-user',
        rowCls: 'nav-tree-badge',
        viewType: 'employee',
        leaf: true
      },
      {
        text: 'Dynamic Component',
        iconCls: 'x-fa fa-edit',
        rowCls: 'nav-tree-badge nav-tree-badge-new',
        viewType: 'dynamic',
        leaf: true
      }
    ]
  }
})

