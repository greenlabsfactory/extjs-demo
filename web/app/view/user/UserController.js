/**
 * Created by krissadewo on 08/09/17.
 */
Ext.define('Admin.view.user.UserController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.user',
    init: function () {
        this.setupView('user');
        this.getStore('user').setProxy(Admin.common.Api.get('User', Api.resource.USER, 0, 10));
    },

    setupView: function (view, params) {
        var toolbar = Ext.create('Admin.view.common.ToolbarView');
        toolbar.down('#addButton').addListener('click', this.onAdd);

        var toolbarMenu = this.getView().down('#toolbar');
        toolbarMenu.add(toolbar);
    },

    onAdd: function () {
        console.log("add new data");
        var form = Ext.create('Admin.view.email.Compose');
        form.show();
        form.center();
    },

    onEdit: function () {
    },

    onDelete: function () {
    },

    onSearch: function () {
    }
});
