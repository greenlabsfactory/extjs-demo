/**
 * Created by krissadewo on 27/09/17.
 */
Ext.define('Admin.view.employee.EmployeeFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.employeeform',
    grid: null,
    init: function () {
    },

    onCancel: function (btn) {
        this.closeWindow(btn);
    },

    closeWindow: function (btn) {
        this.grid.store.reload();
        this.grid.getSelectionModel().deselectAll();

        var win = btn.up('window');
        if (win) {
            win.close();
        }
    },

    onShow: function (self, param) {
        var viewModel = this.getViewModel();
        viewModel.set('employee', null); //set default view model at first window show

        var employee = null;
        if (this.grid.getSelection().length > 0) {
            employee = this.grid.getSelection()[0].data; //fetch model from grid if exist
            if (employee === undefined || employee === null) {
                return;
            }
        }

        viewModel.set('employee', employee);
    },

    onSave: function (btn, e, eOpts) {
        var me = this;
        var form = this.getView().down('#employeeform');
        if (!form.isValid()) {
            return;
        }

        Admin.proxy.Api.save('model.employee', this.getViewModel().get("employee"), {
            callback: function (record, operation, success) {
                console.log(success + " data from server " + record['data'] + " : " + record['status']);
                if (success) {
                    me.closeWindow(btn);
                } else {
                    console.log(operation);
                }
            }
        });
    }
});
