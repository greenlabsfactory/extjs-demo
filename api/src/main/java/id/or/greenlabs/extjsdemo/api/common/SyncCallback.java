package id.or.greenlabs.extjsdemo.api.common;

import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by krissadewo on 9/24/17.
 */
@Component
public class SyncCallback extends HashMap<String, Object> {

    public SyncCallback getCallback(Object data,long total) {
        this.put("data", data);
        this.put("total", total);
        return this;
    }

    public SyncCallback getCallback(Object data) {
        this.put("data", data);
        return this;
    }


    public SyncCallback getCallback(String message, Object data) {
        this.put("data", data);
        this.put("status", message);
        return this;
    }

}
