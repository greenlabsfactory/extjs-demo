package id.or.greenlabs.extjsdemo.api.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.or.greenlabs.extjsdemo.api.util.JsonDateDeserializer;
import id.or.greenlabs.extjsdemo.api.util.JsonDateSerializer;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by krissadewo on 9/23/17.
 */
@Entity
@Getter
@Setter
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String firstName;
    private String lastName;
    private Integer age;
    private String email;

    @JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
    @Column(name = "registration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;

    @JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
    @Column(name = "created_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime = new Date();

    //@JsonBackReference
    //@JsonIgnoreProperties(ignoreUnknown=true)
    @ManyToOne
    @JoinColumn(name = "departement_id")
    private Department department;
}
