/**
 * Dynamic view wrapper page
 */

Ext.define('Admin.view.dynamic.Dynamic', {
  extend: 'Ext.container.Container',
  xtype: 'dynamic',
  alias: 'widget.dynamic',
  margin: '20 20 0 20',
  requires: ['Ext.ux.layout.ResponsiveColumn'],
  layout: 'responsivecolumn',
  items: [
    {
      xtype: 'dynamicForm'
    }
  ]
})
