/**
 * Created by krissadewo on 27/09/17.
 * Base class for configuring common class to use around application
 */
Ext.define("Admin.constants", {
    singleton: true,
    TIMEOUT: 0
});

/**
 * Singleton class for resource configuration
 */
Ext.define("Api.resource", {
    singleton: true,
    alias: 'api.resource',
    USER: 'users',
    EMPLOYEE: 'employees',
    DEPARTMENT: 'departments'
});

/**
 * Singleton class for http method configuration
 */
Ext.define("Api.method", {
    singleton: true,
    alias: 'api.method',
    GET: 'GET',
    POST: 'POST',
    DELETE: 'DELETE',
    PUT: 'PUT'
});

/**
 * Storage place for saving user login data
 */
Ext.define('Admin.data', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'user',
            reference: 'Admin.model.User'
        }
    ],
    proxy: {
        type: 'localstorage',
        id: 'user-data'
    }
});


Ext.define('Admin.data.Token', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'access_token'
        },
        {
            name: 'token_type'
        },
        {
            name: 'refresh_token'
        },
        {
            name: 'expires_in'
        },
        {
            name: 'scope'
        }
    ],
    proxy: {
        type: 'localstorage',
        id: 'token-data'
    }
});