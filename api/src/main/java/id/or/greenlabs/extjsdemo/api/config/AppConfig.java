package id.or.greenlabs.extjsdemo.api.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by krissadewo on 9/25/17.
 */
@ComponentScan(basePackages = {"id.or.greenlabs.extjsdemo.api.filter"})
@Configuration
public class AppConfig {
}
