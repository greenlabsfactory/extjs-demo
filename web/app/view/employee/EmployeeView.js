/**
 * Created by krissadewo on 08/09/17.
 */
Ext.define('Admin.view.employee.Employee', {
    extend: 'Ext.container.Container',
    xtype: 'employee',
    alias: 'widget.employee',
    controller: 'employee',
    margin: '20 20 0 20',
    viewModel: {
        type: 'employee'
    },

    items: [
        {
            xtype: 'panel',
            title: 'Employee',
            frame: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    itemId: 'toolbar'

                },
                {
                    xtype: 'grid',
                    id: 'grid',
                    store: Ext.data.StoreManager.lookup('employee'),
                    layout: {
                        align: 'stretch'
                    },

                    bind: {
                        store: '{employee}'
                    },

                    viewConfig: {
                        preserveScrollOnRefresh: true,
                        preserveScrollOnReload: true,
                        enableTextSelection: true
                    },

                    listeners: {
                        selectionchange: 'onSelectionChange'
                    },

                    columns: [
                        {
                            dataIndex: '',
                            menuDisabled: true,
                            text: '<span class="x-fa fa-heart"></span>',
                            width: 40,
                            renderer: function (value) {
                                return '<span class="x-fa fa-heart' + (value ? '' : '-o') + '"></span>';
                            }
                        },
                        {
                            dataIndex: 'firstName',
                            text: 'First Name',
                            width: "30%",
                            align: 'left'
                        },
                        {
                            dataIndex: 'lastName',
                            text: 'Last Name',
                            width: "30%",
                            align: 'left'
                        },
                        {
                            dataIndex: 'email',
                            text: 'Email',
                            width: "10%",
                            align: 'left'
                        },
                        {
                            dataIndex: 'age',
                            text: 'Age'
                        },
                        {
                            dataIndex: 'department',
                            width: "20%",
                            text: 'Department Name',
                            renderer: function (value, metaData, record) {
                                return record.data.department.name;
                            }
                        },
                        {
                            dataIndex: 'registrationDate',
                            width: "20%",
                            text: 'Registration Date',
                            renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s'),
                            align: 'center'
                        }
                    ],

                    bbar: {
                        xtype: 'pagingtoolbar',
                        displayInfo: true,
                        displayMsg: 'Displaying topics {0} - {1} of {2}',
                        emptyMsg: "No topics to display"
                    }
                }
            ]

        }
    ]

});
